package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("xiaowang");
        ChildEntity childEntity = new ChildEntity("xiaoming");
        childEntity.setParentEntity(parentEntity);
        flushAndClear(entityManager ->{
            parentEntityRepository.save(parentEntity);
            childEntityRepository.save(childEntity);
        });
        ChildEntity fetchchildren = childEntityRepository.findById(childEntity.getId()).get();
        assertEquals("xiaoming",fetchchildren.getName());
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("xiaowang");
        ChildEntity childEntity = new ChildEntity("xiaoming");
        childEntity.setParentEntity(parentEntity);
        flushAndClear(entityManager ->{
            parentEntityRepository.save(parentEntity);
            childEntityRepository.save(childEntity);
            childEntity.setParentEntity(null);
            childEntityRepository.save(childEntity);
        });
        ChildEntity fetchchildren = childEntityRepository.findById(childEntity.getId()).get();
        ParentEntity fetchparent = parentEntityRepository.findById(parentEntity.getId()).get();
        assertNull(fetchchildren.getParentEntity());
        assertEquals("xiaoming",fetchchildren.getName());
        assertEquals("xiaowang",fetchparent.getName());
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("xiaowang");
        ChildEntity childEntity = new ChildEntity("xiaoming");
        childEntity.setParentEntity(parentEntity);
        flushAndClear(entityManager ->{
            childEntityRepository.save(childEntity);
            parentEntityRepository.save(parentEntity);
            parentEntityRepository.deleteById(parentEntity.getId());
            childEntityRepository.deleteById(childEntity.getId());
        });
        Optional<ChildEntity> fetchchildren = childEntityRepository.findById(1L);
        Optional<ParentEntity> fetchparent = parentEntityRepository.findById(1L);
        fetchchildren.isPresent();
        fetchparent.isPresent();
        // --end-->
    }
}